from __future__ import unicode_literals
from django.db import models
from org.models import Organization

# Create your models here.

class Project(models.Model):
	name = models.CharField(max_length=255)
	client = models.CharField(max_length=255)
	organization = models.ForeignKey(Organization, on_delete=models.CASCADE)

class Team(models.Model):
	name = models.CharField(max_length=255)
	project = models.ForeignKey(Project, on_delete=models.CASCADE)
	organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
