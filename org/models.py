from __future__ import unicode_literals
from django.contrib.auth.models import User as user
from django.db import models
from django.db.models.signals import post_save

# Create your models here.

class Organization(models.Model):
	"""
	Represents an organization
	"""
	name = models.CharField(max_length=255, null=True, blank=False)

	def __str__(self):
		return "%s" % self.name

class Employee(models.Model):
	"""
	Represents employee belonging to organization
	"""
	from project.models import Team,Project
	Employee = models.OneToOneField(user, null=False, blank=False, on_delete=models.CASCADE, related_name='user')
	name = models.CharField(max_length=255, null=True, blank=False)
	email = models.EmailField(null=True, blank=False)
	team = models.ForeignKey(Team, on_delete=models.CASCADE)
	projects = models.ManyToManyField(Project)
	organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
	def __str__(self):
		return "%s" % self.name


def create_user(sender, instance, created, **kwargs):
	user, created = Employee.objects.get_or_create(Employee=instance)
	user.name = instance.first_name
	user.email = instance.email
	user.save()
	if(created):
		print "user created"
	else:
		print "updated user"


post_save.connect(create_user, sender=user)



